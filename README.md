A simple Ansible playbook to execute a command to all remote hosts

For example, if you wan to execute `echo Hello world` run this

```
ansible-playbook -i hosts -e 'command="echo Hello world"' execute-command-playbook.yml
```
